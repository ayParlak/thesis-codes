import copy
import json
import sys
from itertools import chain, combinations

def powerset(iterable):
  xs = list(iterable)
  mapset = chain.from_iterable( combinations(xs,n) for n in range(len(xs)+1) )  
  return mapset
input_k = 3
mapset = map(set, powerset(range(input_k)))
markowski = {}
for a in sorted(mapset):
    for b in sorted(mapset):
        for c in sorted(mapset):
            tempSet = {'A':a,'B':b,'C':c,'M':0}
            tempKey = ''
            if len(a) == 0:
                tempKey += 'A'+'-1:'
            for i in a:
                tempKey += 'A'+str(i)+':'
            if len(b) == 0:
                tempKey += 'B'+'-1:'
            for j in b:
                tempKey += 'B'+str(j)+':'
            if len(c) == 0:
                tempKey += 'C'+'-1:'
            for k in c:
                tempKey += 'C'+str(k)+':'
            markowski[tempKey]=tempSet

kTableInitial = {}
for i in range(input_k*2):
	kTableInitial[i]=''

def printTreeWithMT(incomeNode):
	if incomeNode.type == 'leaf':
		print incomeNode.level,'level ',incomeNode.type, '-',incomeNode.kTableBefore , incomeNode.kTable, '--->' ,incomeNode.operation,incomeNode.markowskiTable
		return
	else:
		for incomeChildNode in incomeNode.childs:
			printTreeWithMT(incomeChildNode)
	print incomeNode.level,'level ',incomeNode.type, '-',incomeNode.kTableBefore , incomeNode.kTable,'--->' ,incomeNode.operation,incomeNode.markowskiTable
	return

def printTree(incomeNode):
	if incomeNode.type == 'leaf':
		print incomeNode.level,'level ',incomeNode.type, '-',incomeNode.kTableBefore , incomeNode.kTable, '--->' ,incomeNode.operation
		return
	else:
		for incomeChildNode in incomeNode.childs:
			printTree(incomeChildNode)
	print incomeNode.level,'level ',incomeNode.type, '-' ,incomeNode.kTableBefore , incomeNode.kTable, '--->' ,incomeNode.operation
	return

def getLevel(incomeNode,level):
	if incomeNode.type == 'leaf':
		incomeNode.level = level
		return
	else:
		for incomeChildNode in incomeNode.childs:
			getLevel(incomeChildNode,level+1)
	incomeNode.level = level
	return

def calculateModel(incomeNode):
	if incomeNode.type == 'l-node':
		print incomeNode.type,incomeNode.level,incomeNode.kTable,incomeNode.model
		return
	else:
		for incomeChildNode in incomeNode.childs:
			calculateModel(incomeChildNode)
	if incomeNode.type == 'Q-node':
		print incomeNode.type,incomeNode.level,incomeNode.kTable,incomeNode.model
		return
	elif incomeNode.type == 'p-node':
		print incomeNode.type,incomeNode.level, incomeNode.operation,incomeNode.kTable,incomeNode.model
		return
	elif incomeNode.type == 'n-node':
		print incomeNode.type,incomeNode.level, incomeNode.operation,incomeNode.kTable,incomeNode.model
		return




class Node(object):
    def __init__(self):
    	self.text = None
    	self.type = None
    	self.parent = None
    	self.childs = []
    	self.elements = []
        self.kTable = copy.deepcopy(kTableInitial)
        self.markowskiTable = copy.deepcopy(markowski)
        self.kTableBefore = {}
        self.kTableAfter = {}
        self.labelFrom = None
        self.labelTo = None
        self.operation = ''
        self.level = None
        self.model = self.markowskiTable['A-1:B-1:C-1:']['M']
class Arc(object):
	def __init__(self):
		self.nfrom = None
		self.nto = None
		self.nNodes = []
		self.isMatched = None
#read arcs
tempArcs = open(str(sys.argv[1]),'r')
tempArcslist = tempArcs.readlines()
arcs = []
arcsMap = {}
for i in range(len(tempArcslist)):
	if i == 0:
		continue
	tempArc = tempArcslist[i].replace('\n','').replace('a ','').split(' ')
	arc = Arc()
	arc.nfrom = tempArc[0]
	arc.nto = tempArc[1]
	key = arc.nfrom +','+arc.nto
	arcsMap[key]= arc
	arcs.append(arc)

file = open(str(sys.argv[2]), 'r')
treeList = file.readlines()
treeList.reverse() 
nlines = len(treeList)
rootNode = None
nodeList = []
nodeList2 = []
tempParentNodeDict = {};
tempChildNodeDict = {};

for i in range(nlines):
	treeList[i] = treeList[i].replace('c ','').replace('\n','').replace('}}{{','}};{{').replace('}{','}:{')

for i in range(nlines-1):	
	tempParentNodeDict = copy.copy(tempChildNodeDict);
	tempChildNodeDict = {};						# in the firts line
	parentLevelVertexsText = treeList[i].split(';')
	childLevelVertexsText = treeList[i+1].split(';')
	tempList = []
	if  rootNode == None :
	  for j in range(len(parentLevelVertexsText)):
		node = Node()
		node.text = parentLevelVertexsText[j]
		node.elements = parentLevelVertexsText[j].replace('{{','{').replace('}}','}').split(':')
		for kNum in range(len(node.elements)):
			node.kTable[kNum] = node.elements[kNum].replace('{','').replace('}','')
		if( i == 0): 		
			node.type = 'r-node'
			rootNode = node
			rootNode.level = 0
		tempParentNodeDict[node.text] = node
		nodeList.append(node)
	for j in range(len(childLevelVertexsText)):
		node = Node()
		node.text = childLevelVertexsText[j]
		node.elements = childLevelVertexsText[j].replace('{{','{').replace('}}','}').split(':')
		#print node.elements
		for kNum in range(len(node.elements)):
			node.kTable[kNum] = node.elements[kNum].replace('{','').replace('}','')
		if i == nlines-2:
			node.type = 'l-node'
		else:
			node.type = 'Q-node'
		tempChildNodeDict[node.text] = node
		nodeList.append(node)

###### The main codinf ##############################

	parentKeys = tempParentNodeDict.keys()
	childKeys = tempChildNodeDict.keys()    
	for parkey in parentKeys:
		parentNode = tempParentNodeDict[parkey]
		nodeList2.append (parentNode)
		for chkey in childKeys:

			if len( set(parkey.replace('{','').replace('}','').replace(':','').replace(',','')) & set(chkey.replace('{','').replace('}','').replace(':','').replace(',','')) ) > 0 :				
				childNode = tempChildNodeDict[chkey]
				childNode.parent = parentNode
				parentNode.childs.append(childNode)
				currentChild = childNode
				#fixed  fix one of the merge element label
				kTableFixed = []
				for parentKtableKey in parentNode.kTable.keys():
					childLabels = []
					for childKtableKey in childNode.kTable.keys():
						if(len( set(parentNode.kTable[parentKtableKey].replace(',','')) & set(childNode.kTable[childKtableKey].replace(',',''))) > 0 ):
							childLabels.append(childKtableKey)
					if childLabels.count(parentKtableKey) > 0:
						kTableFixed.append(parentKtableKey)
					elif len(childLabels)>0:
						firstChild = childLabels[0]
						temp = childNode.kTable[parentKtableKey]
						childNode.kTable[parentKtableKey] = childNode.kTable[firstChild]
						childNode.kTable[firstChild] = temp
				#fixed

				#n-node find
				if childNode.type == 'Q-node':
					for childKtableFrom in childNode.kTable.keys():
						for childKtableTo in childNode.kTable.keys():
							if childKtableFrom == childKtableTo:
								continue
							control = True
							for nfrom in childNode.kTable[childKtableFrom].split(','):
								for nto in childNode.kTable[childKtableTo].split(','):
									key= nfrom + ',' +nto
									if not arcsMap.has_key(key):
										control = False
										break
									else:
										arc = arcsMap[key]
										if arc.isMatched:
											control = False
											break
								if not control:
									break
							if control:
								newNode= Node()
								newNode.operation = str(childKtableFrom)+'->'+str(childKtableTo)
								newNode.type = 'n-node'
								newNode.labelFrom = childKtableFrom
								newNode.labelTo = childKtableTo
								newNode.kTable = childNode.kTable
								newNode.parent = parentNode
								parentNode.childs.append(newNode)
								newNode.childs.append(currentChild)
								currentChild.parent = newNode
								parentNode.childs.remove(currentChild)
								currentChild = newNode
								for nfrom in childNode.kTable[childKtableFrom].split(','):
									for nto in childNode.kTable[childKtableTo].split(','):
										key= nfrom + ',' +nto
										arc =arcsMap[key]
										arc.isMatched=True
				
				#n-node find

				#p-node find
				for parentKtableKey in parentNode.kTable.keys():
					for childKtableKey in childNode.kTable.keys():
							if(len( set(parentNode.kTable[parentKtableKey].replace(',','')) & set(childNode.kTable[childKtableKey].replace(',',''))) > 0 and childKtableKey != parentKtableKey):
								#childNode.operation = childNode.operation + str(childKtableKey) + '->' + str(parentKtableKey)+'   '
								newNode = Node()
								newNode.operation = childNode.operation + str(childKtableKey) + '->' + str(parentKtableKey)
								newNode.type = 'p-node'
								newNode.labelFrom = childKtableKey
								newNode.labelTo = parentKtableKey
								newNode.kTable = copy.deepcopy(currentChild.kTable)
								newNode.kTableBefore = copy.deepcopy(currentChild.kTable)
								if newNode.kTable.has_key(newNode.labelTo):
									newNode.kTable[newNode.labelTo] = newNode.kTable[newNode.labelTo] +','+ newNode.kTable[newNode.labelFrom]
									newNode.kTable[newNode.labelFrom] = ''
								else:
									newNode.kTable[newNode.labelTo] = newNode.kTable[newNode.labelFrom]
									newNode.kTable[newNode.labelFrom] = ''

								newNode.parent = parentNode
								parentNode.childs.append(newNode)

								newNode.childs.append(currentChild)
								currentChild.parent = newNode

								parentNode.childs.remove(currentChild)

								currentChild = newNode
								

getLevel(rootNode,0)
printTree(rootNode)
