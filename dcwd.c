/************************************************************************
*  This is encoder for directed graph by Aykut Parlak (June 6 2016)    *
*  modified code from cwd-encode by Marijn Heule (October 15th, 2013)  *
*  converts a graph formula in DIMACS format and a parameter k into    *
*  a CNF (creates 'sat.cnf' file) formula in DIMACS format that is     *
*  satisfiable if and only if corresponding graph has a clique-width   *
*  less of equal to k.                                                  *
************************************************************************/

#include <stdio.h>
#include <stdlib.h>

//#define DIRECT
#define DERIVATION
#define CARDINALITY     // MUST BE ON WITH DERIVATION
//#define LINEAR

int **arc, **OrderedPair; //**arc
int var_k, var_n, nArcs, nOrderedPair, var_t; //nArcs
int offset = 0, linear;


void addArc (int vertexU, int vertexV) 
{
   if (!arc[ vertexU ][ vertexV ]) 
    arc[ vertexU ][ vertexV ] = ++nArcs; 
}


inline int getComponent (int vertexU, int vertexV, int time) 
{
  return nOrderedPair*time + OrderedPair[ vertexU ][ vertexV ]; 
}

inline int getGroup (int vertexU, int vertexV, int time) 
{
    return nOrderedPair*(var_t + 1 + time) + OrderedPair[ vertexU ][ vertexV ]; 
}

#ifdef DIRECT
    inline int getLabel (int vertex, int color, int time) 
    {
        return 2 * nOrderedPair * (var_t+1) + var_n * var_k * time + (vertex-1) * var_k + color; 
    }
#else
    #ifdef DERIVATION
        inline int getRepresentative (int vertex, int time) 
        {
         return 2 * nOrderedPair * (var_t+1) + var_n * time + vertex; 
        }

        inline int getClique (int vertex, int color, int time) 
        {
         return 2 * nOrderedPair * (var_t+1) + var_n * (var_t+1) + 
           var_n * (var_k-1) * time + (vertex-1) * (var_k-1) + color; 
        }
    #else
        inline int getRepresentative (int vertex, int time) 
        {
         return offset + var_n * time + vertex; 
        }

        inline int getClique (int vertex, int color, int time) 
        {
         return offset + var_n * (var_t+1) + var_n * (var_k-1) * time + (vertex-1) * (var_k-1) + color; 
        }
    #endif
#endif

inline int getLinear (int vertex, int time) 
{
    return linear + time * var_n + vertex; 
}

int main (int argc, char **argv) 
{
   int i, j, tmp;
   int u, v, w, x, t;
   int  nArcGraph, nVars, nClauses = 0; 
   int vertexU, vertexV;
   int shorter = 0;
   FILE *fp;
   fp = fopen("temp.cnf", "w+");

   // right argument
   if (argc < 3) 
      { 
         fprintf(fp,"wrong input: ./cwd-encode FILE COLORS\n"); 
         exit(0); 
      }
   
   // if more extra argument
      #ifndef DERIVATION
      if (argc > 3) offset = atoi  (argv[3]);
      #else    
      if (argc > 3) shorter = atoi  (argv[3]);
      #endif

   FILE *file = fopen (argv[1], "r");
   var_k    = atoi  (argv[2]);

   
   //read head of problem file for n and nArcGraph
   do 
   { 
        tmp = fscanf (file, " p arc %i %i \n", &var_n, &nArcGraph);
        if (tmp > 0 && tmp != EOF) break; 
        tmp = fscanf (file, "%*s\n"); 
   }
   while (tmp != 2 && tmp != EOF);

   //if one k=1
   if (var_k == 1) 
   {
      if (nArcGraph == 0) fprintf(fp,"p cnf 0 0\n");
      else             fprintf(fp,"p cnf 0 1\n0\n");
      return 0;
   }

   
  nArcs   = 0;
  var_t    = var_n - var_k + 1 - shorter; // t=n-k-1
  fprintf(fp,"c shorter = %i (%i)\n", shorter, argc);

  
  // Arc varible initialization arc[u][v]=0
    arc = (int **) malloc (sizeof (int*) * (var_n+1));
    for (u = 1; u <= var_n; u++) 
    arc[u] = (int *) malloc (sizeof (int) * (var_n+1) );

    for (u = 1; u <= var_n; u++)
      for (v = 1; v <= var_n; v++)
         arc[u][v] = 0;
   
  // Insert arc with value number of nArcs
    for (i = 1; i <= nArcGraph; i++) 
    {
      tmp = fscanf (file, " a %i %i \n", &vertexU, &vertexV);
      addArc (vertexU, vertexV);
    }
   

   // OrderedPair initialization
      OrderedPair = (int **) malloc (sizeof (int*) * (var_n+1));
      for (u = 1; u <= var_n; u++) 
         OrderedPair[u] = (int *) malloc (sizeof (int) * (var_n+1) );

      nOrderedPair = 0;
      for (u = 1; u <= var_n; u++)
       for (v = u + 1; v <= var_n; v++)
          OrderedPair[u][v] = ++nOrderedPair;
   
   //nClauses = 0     

  // nVar     
  nVars     = (var_t + 1) * 2 * nOrderedPair; // number of variable

  #ifdef CARDINALITY
    nVars    += (var_t + 1) * var_n * var_k;
  #endif

  #ifdef LINEAR
    linear = nVars;
    nVars += var_t * var_n;
  #endif

  fprintf(fp,"c groups %i, colors %i\n", nOrderedPair, var_k);
  //fprintf(fp,"p cnf %i %i\n", nVars, nClauses);
   //--------------------------------------------------------------------------------------------    
  #ifdef DERIVATION
  // initialization and goal clauses ,D1
    for (u = 1; u <= var_n; u++)
      for (v = u+1; v <= var_n; v++)
        fprintf(fp,"-%i 0\n%i 0\n", getComponent (u, v, 0), getComponent (u, v, var_t));
      //D2
    for (t = 0; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
        for (v = u+1; v <= var_n; v++)
          fprintf(fp,"%i -%i 0\n", getComponent (u, v, t), getGroup (u, v, t));

    // Arc property clauses
      for (t = 1; t <= var_t; t++)
        for (u = 1; u <= var_n; u++)
          for (v = u+1; v <= var_n; v++)  
          {
            if (arc[u][v]) fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, v, t)); 
            if (arc[v][u]) fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, v, t)); 
          }
        
    // neighborhood property clauses Arc
      for (t = 1; t <= var_t; t++)
        for (u = 1; u <= var_n; u++)
          for (v = u+1; v <= var_n; v++)
          {
            if (arc[u][v])
              for (w = 1; w <= var_n; w++) 
              {
                if ((u == w) || (v == w)) continue;
                if (!arc[u][w]) 
                {
                  if (v < w) 
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (v, w, t));
                  else       
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, v, t)); 
                }
                
                if (!arc[w][v]) 
                {
                  if (u < w) 
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, w, t));
                  else       
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, u, t)); 
                }
              }
            if (arc[v][u])
              for (w = 1; w <= var_n; w++) 
              {
                if ((u == w) || (v == w)) continue;
                if (!arc[w][u]) 
                {
                  if (v < w) 
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (v, w, t));
                  else       
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, v, t)); 
                }
                if (!arc[v][w]) 
                {
                  if (u < w) 
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (u, w, t));
                  else       
                    fprintf(fp,"%i -%i 0\n", getComponent (u, v, t-1), getGroup (w, u, t)); 
                }
              }
          }     
    
    // path property clauses Arc
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
        for (v = u+1; v <= var_n; v++)
        {
          if (arc[u][v])
            for (w = 1; w <= var_n; w++)
              if (arc[u][w] && (v != w))
                for (x = 1; x <= var_n; x++)
                  if (arc[x][v] && (u != x) && !arc[x][w] && (w != x)) 
                  {
                    if (u < x) 
                      fprintf(fp,"%i -%i ", getComponent(u, v, t-1), getGroup (u, x, t));
                    else       
                      fprintf(fp,"%i -%i ", getComponent(u, v, t-1), getGroup (x, u, t));
                    if (v < w) 
                      fprintf(fp,"-%i 0\n", getGroup (v, w, t));
                    else       
                      fprintf(fp,"-%i 0\n", getGroup (w, v, t));
                  }
               
          if (arc[v][u])
            for (w = 1; w <= var_n; w++)
              if (arc[w][u] && (v != w))
               for (x = 1; x <= var_n; x++)
                if (arc[v][x] && (u != x) && !arc[w][x] && (w != x)) 
                  {
                    if (u < x) 
                      fprintf(fp,"%i -%i ", getComponent(u, v, t-1), getGroup (u, x, t));
                    else       
                      fprintf(fp,"%i -%i ", getComponent(u, v, t-1), getGroup (x, u, t));
                    if (v < w) 
                      fprintf(fp,"-%i 0\n", getGroup (v, w, t));
                    else       
                      fprintf(fp,"-%i 0\n", getGroup (w, v, t));
                  }
          }
   

   // component time clauses
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
       for (v = u+1; v <= var_n; v++)
          fprintf(fp,"-%i %i 0\n", getComponent (u, v, t-1), getComponent (u, v, t));

   // group time clauses
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
       for (v = u+1; v <= var_n; v++)
          fprintf(fp,"-%i %i 0\n", getGroup (u, v, t-1), getGroup (u, v, t));

   // component transitive clauses
   for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
       for (v = u+1; v <= var_n; v++)
          for (w = v+1; w <= var_n; w++) 
           {
             fprintf(fp,"-%i -%i %i 0\n", getComponent (u, v, t), getComponent (u, w, t), getComponent (v, w, t));
             fprintf(fp,"-%i -%i %i 0\n", getComponent (u, v, t), getComponent (v, w, t), getComponent (u, w, t));
             fprintf(fp,"-%i -%i %i 0\n", getComponent (u, w, t), getComponent (v, w, t), getComponent (u, v, t));
            }
   #endif

   #ifdef DIRECT
   // label clauses (var_t+1) * var_n * ((var_k*(var_k-1)/2)+1)
    for (t = 1; t <= var_t; t++)
      for (v = 1; v <= var_n; v++) 
      {
        for (i = 1; i <= var_k; i++)
          fprintf(fp,"%i ", getLabel (v, i, t));
        fprintf(fp,"0\n");
        for (i = 1; i <= var_k; i++)
          for (j = i+1; j <= var_k; j++)
            fprintf(fp,"-%i -%i 0\n", getLabel (v, i, t), getLabel (v, j, t));
      }

   // group relation clauses
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
       for (v = u+1; v <= var_n; v++)
          for (i = 1; i <= var_k; i++) 
           {
               fprintf(fp,"-%i -%i -%i %i 0\n", getComponent (u, v, t), getLabel (u, i, t), getLabel (v, i, t), getGroup (u, v, t));
               fprintf(fp,"-%i -%i %i 0\n", getGroup (u, v, t), getLabel (u, i, t), getLabel (v, i, t));
               fprintf(fp,"-%i %i -%i 0\n", getGroup (u, v, t), getLabel (u, i, t), getLabel (v, i, t));
          }
   #else

   #ifdef DERIVATION
   // group transitive clauses
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
       for (v = u+1; v <= var_n; v++)
          for (w = v+1; w <= var_n; w++) 
           {
               fprintf(fp,"-%i -%i %i 0\n", getGroup (u, v, t), getGroup (u, w, t), getGroup (v, w, t));
               fprintf(fp,"-%i -%i %i 0\n", getGroup (u, v, t), getGroup (v, w, t), getGroup (u, w, t));
               fprintf(fp,"-%i -%i %i 0\n", getGroup (u, w, t), getGroup (v, w, t), getGroup (u, v, t));
            }
   #endif

   #ifdef LINEAR
    for (t = 0; t < var_t; t++)
      for (u = 1; u <= var_n; u++)
        for (v = u + 1; v <= var_n; v++) 
        {
           fprintf(fp,"%i -%i -%i 0\n", getComponent (u,v,t), getLinear (u,t), getLinear (v,t));
           fprintf(fp,"-%i %i 0\n", getComponent (u,v,t), getLinear (u,t));
           fprintf(fp,"-%i %i 0\n", getComponent (u,v,t), getLinear (v,t)); 
        }
   #endif

   #ifdef CARDINALITY
   // representative clauses
    for (t = 1; t <= var_t; t++)
      for (v = 1; v <= var_n; v++) 
      {
       for (u = 1; u < v; u++)
         fprintf(fp,"%i ", getGroup (u,v,t));
         fprintf(fp,"%i 0\n", getRepresentative (v,t)); 
      }

    for (t = 1; t <= var_t; t++)
      for (v = 1; v <= var_n; v++)
       for (u = 1; u < v; u++)
          fprintf(fp,"-%i -%i 0\n", getGroup (u, v, t), getRepresentative (v, t));

   // clashing clique clauses
    for (t = 1; t <= var_t; t++)
      for (u = 1; u <= var_n; u++)
        for (v = u+1; v <= var_n; v++) 
        {
          fprintf(fp,"-%i -%i -%i -%i 0\n", getComponent (u, v, t), getRepresentative (u, t), getRepresentative (v, t), getClique (u, var_k-1, t));
          for (i = 1; i < var_k-1; i++)
             fprintf(fp,"-%i -%i -%i %i -%i 0\n", getComponent (u, v, t), getRepresentative (u, t), getRepresentative(v, t), getClique (v, i+1, t), getClique(u, i, t));
          fprintf(fp,"-%i -%i -%i %i 0\n", getComponent (u, v, t), getRepresentative (u, t), getRepresentative (v, t), getClique (v, 1, t)); }
   #endif
   #endif
    
    fclose(fp);
    fp = fopen("temp.cnf", "r");

    int nClausesc = 0;
    char ch;
    
    while(!feof(fp))
    {
      ch = fgetc(fp);
      if(ch == '\n')
      {
        nClausesc++;
      }
    }
    //printf("TEST::: %i %i\n", nVars, (nClausesc-3));
    nClausesc = nClausesc-2;
    fclose(fp);

    FILE *fp2;
    fp2 = fopen("sat.cnf", "w+");
    fp = fopen("temp.cnf", "r");
    
    fprintf(fp2,"p cnf %i %i\n", nVars, nClausesc);
    int a;
    while(!feof(fp)) 
    {
      a = fgetc(fp);
      //printf("%i\n", a);
      if(a >= 0 )fputc(a, fp2);
   } 

    fclose(fp);
    fclose(fp2);
}

