import copy
import json
import sys
from itertools import chain, combinations
import collections, operator

class frozendict(collections.Mapping):

    def __init__(self, *args, **kwargs):
        self.__dict = dict(*args, **kwargs)
        self.__hash = None

    def __getitem__(self, key):
        return self.__dict[key]

    def copy(self, **add_or_replace):
        return frozendict(self, **add_or_replace)

    def __iter__(self):
        return iter(self.__dict)

    def __len__(self):
        return len(self.__dict)

    def __repr__(self):
        return '<frozendict %s>' % repr(self.__dict)

    def __hash__(self):
        if self.__hash is None:
            self.__hash = reduce(operator.xor, map(hash, self.iteritems()), 0)

        return self.__hash

calculater=0
input_k = 4

def powerset(iterable):
  xs = list(iterable)
  mapset = chain.from_iterable( combinations(xs,n) for n in range(len(xs)+1) )  
  return mapset

def printNumModel(inoutNode):
    for i in inoutNode.markowskiTable:
        if not i['A'] and not i['B'] and not i['C']:
            print 'A',",".join(str(x) for x in i['A']),';','B',",".join(str(x) for x in i['B']),';','C',",".join(str(x) for x in i['C']),';',inoutNode.markowskiTable[i]['NumModel']
        #print  ",".join(A+str(x) for x in i['A']),';',",".join(str(x) for x in i['B']),';',",".join(str(x) for x in i['C']),';',inoutNode.markowskiTable[i]['NumModel']
        #print  ":".join('A'+str(x) for x in i['A']),":".join('B'+str(x) for x in i['B']),":".join('C'+str(x) for x in i['C']),';',inoutNode.markowskiTable[i]['NumModel']

        #print i,' ',inoutNode.markowskiTable[i]['NumModel']

def getElement(node,list1,list2,list3):
    print node.markowskiTable[frozendict({'A':frozenset(list1),'B':frozenset(list2),'C':frozenset(list3)})]['NumModel']

mapset = map(set, powerset(range(input_k)))
markowski = {}
for a in sorted(mapset):
    for b in sorted(mapset):
        for c in sorted(mapset):
            tempSet = {'A':str(a),'B':str(b),'C':str(c),'NumModel':0,'Checked':0,'truthValue':False}
            tempKey = frozendict({'A':frozenset(a),'B':frozenset(b),'C':frozenset(c)})
            markowski[tempKey]=tempSet

kTableInitial = {}
for i in range(input_k):
    kTableInitial[i]=''

class Node(object):
    def __init__(self):
        self.text = None
        self.type = None
        self.parent = None
        self.childs = []
        self.elements = []
        self.kTable = copy.deepcopy(kTableInitial)
        self.markowskiTable = copy.deepcopy(markowski)
        self.kTableBefore = {}
        self.kTableAfter = {}
        self.labelFrom = None
        self.labelTo = None
        self.operation = ''
        self.level = None
        self.model = None

def setMarkowskiTableCheckedTo0(theNode):
    for key in theNode.markowskiTable:
        theNode.markowskiTable[key]['Checked']=0

def n_operation( i_from,j_to,theNode):
    markowskiTable_old = copy.deepcopy(theNode.markowskiTable)
    setMarkowskiTableCheckedTo0(theNode)
    for key in theNode.markowskiTable:
        if ('A'+i_from) in key and theNode.markowskiTable[key]['Checked'] == 0:
            theNode.markowskiTable[key]['Checked'] = 1
            print ('A'+i_from),'in',key,'-->',theNode.markowskiTable[key]

    for key in theNode.markowskiTable:
        if ('B'+j_to) in key and theNode.markowskiTable[key]['Checked'] == 0:
            theNode.markowskiTable[key]['Checked'] = 1
            print ('B'+j_to),'in',key ,'-->',theNode.markowskiTable[key]
    for key in theNode.markowskiTable:
        if theNode.markowskiTable[key]['Checked'] == 0:
            print 'test'

def initialMarkowskiforLeafNode(theNode):
    label= None
    vertex = None
    vertexType = None
    n_variable = None
    for i in theNode.kTable:
        if  theNode.kTable[i]:
            label = i
            vertex = theNode.kTable[i]
            vertexType = IsClause[vertex]
            n_variable = 1 if vertexType == 'variable' else 0;
    
    if vertexType == 'variable':
        for key in theNode.markowskiTable:
            if not label in key['B'] and not label in key['C']:
                theNode.markowskiTable[key]['truthValue']=True
                theNode.markowskiTable[key]['NumModel']=2 

            elif label in key['B'] and not label in key['C']:
                theNode.markowskiTable[key]['truthValue']=True
                theNode.markowskiTable[key]['NumModel']=1

            elif not label in key['B'] and label in key['C']:
                theNode.markowskiTable[key]['truthValue']=True
                theNode.markowskiTable[key]['NumModel']=1 
            else :
                theNode.markowskiTable[key]['truthValue']=False
                theNode.markowskiTable[key]['NumModel']=0

    if vertexType == 'clause':
        for key in theNode.markowskiTable:
            if label in key['A']:
                theNode.markowskiTable[key]['truthValue']=True
                theNode.markowskiTable[key]['NumModel']=1
            else:
                theNode.markowskiTable[key]['truthValue']=False
                theNode.markowskiTable[key]['NumModel']=0

def disjointUnion(node1,node2):
    unionNode = Node()
    for key in node1.markowskiTable:
        node1.markowskiTable[key]
        node2.markowskiTable[key]

        unionNode.markowskiTable[key]['NumModel']= node1.markowskiTable[key]['NumModel'] * node2.markowskiTable[key]['NumModel']
    return unionNode

def n_positive(node1,label_i,label_j):
    tempMakowskiTable=copy.deepcopy(markowski)

    for key in tempMakowskiTable:
        if label_i in key['A']:
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key]['NumModel']
        elif label_j in key['B']:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            key_old = key.copy(A= fsetA)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key_old]['NumModel']
        else:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            listB=list(key['B'])
            listB.append(label_j)
            fsetB=frozenset(listB)
            key_old_1 = key.copy(A=fsetA,B=fsetB)
            key_old_2 = key.copy(B=fsetB)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key]['NumModel']+node1.markowskiTable[key_old_1]['NumModel']-node1.markowskiTable[key_old_2]['NumModel']
    node1.markowskiTable=copy.deepcopy(tempMakowskiTable)

#i is always clause and j is always variable
def n_negative(node1,label_i,label_j):
    tempMakowskiTable=copy.deepcopy(markowski)

    for key in tempMakowskiTable:
        if label_i in key['A']:
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key]['NumModel']
        elif label_j in key['C']:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            key_old = key.copy(A= fsetA)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key_old]['NumModel']
        else:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            listC=list(key['C'])
            listC.append(label_j)
            fsetC=frozenset(listC)
            key_old_1 = key.copy(A=fsetA,C=fsetC)
            key_old_2 = key.copy(C=fsetC)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key]['NumModel']+node1.markowskiTable[key_old_1]['NumModel']-node1.markowskiTable[key_old_2]['NumModel']
    node1.markowskiTable=copy.deepcopy(tempMakowskiTable)

#from i to j
def p_operation(node1,label_i,label_j):
    tempMakowskiTable=copy.deepcopy(markowski)

    for key in tempMakowskiTable:
        if label_i in key['B'] or label_i in key['C']:
            tempMakowskiTable[key]['NumModel']= 0
            continue

        list_A_prime = list(key['A'])
        if label_j in key['A']:
            list_A_prime.append(label_i)
        elif label_i in list_A_prime: 
                list_A_prime.remove(label_i)
        fsetA_prime = frozenset(list_A_prime)

        if not label_j in key['B'] or not label_j in key['C']:
            key_old = key.copy(A=fsetA_prime)
            tempMakowskiTable[key]['NumModel'] = node1.markowskiTable[key_old]['NumModel']

        elif label_j in key['B'] or not label_j in key['C']:
            listB = list(key['B'])
            listB1 = list(listB)
            listB1.append(label_i)
            listB1.remove(label_j)
            listB2 = list(listB)
            listB3 = list(listB)
            listB3.append(label_i)

            fsetB1 = frozenset(listB1)
            fsetB2 = frozenset(listB2)
            fsetB3 = frozenset(listB3)

            key_old_B1 = key.copy(A=fsetA_prime,B=fsetB1)
            key_old_B2 = key.copy(A=fsetA_prime,B=fsetB2)
            key_old_B3 = key.copy(A=fsetA_prime,B=fsetB3)

            tempMakowskiTable[key]['NumModel'] = node1.markowskiTable[key_old_B1]['NumModel'] + node1.markowskiTable[key_old_B2]['NumModel'] -node1.markowskiTable[key_old_B3]['NumModel']

        elif not label_j in key['B'] or label_j in key['C']:
            listC = list(key['C'])
            listC1 = list(listC)
            listC1.append(label_i)
            listC1.remove(label_j)
            listC2 = list(listC)
            listC3 = list(listC)
            listC3.append(label_i)

            fsetC1 = frozenset(listC1)
            fsetC2 = frozenset(listC2)
            fsetC3 = frozenset(listC3)

            key_old_C1 = key.copy(A=fsetA_prime,C=fsetC1)
            key_old_C2 = key.copy(A=fsetA_prime,C=fsetC2)
            key_old_C3 = key.copy(A=fsetA_prime,C=fsetC3)

            tempMakowskiTable[key]['NumModel'] = node1.markowskiTable[key_old_C1]['NumModel'] + node1.markowskiTable[key_old_C2]['NumModel'] - node1.markowskiTable[key_old_C3]['NumModel']

        elif label_j in key['B'] or label_j in key['C']:
            listB = list(key['B'])
            listB1 = list(listB)
            listB1.append(label_i)
            listB1.remove(label_j)
            listB2 = list(listB)
            listB3 = list(listB)
            listB3.append(label_i)

            fsetB1 = frozenset(listB1)
            fsetB2 = frozenset(listB2)
            fsetB3 = frozenset(listB3)

            listC = list(key['C'])
            listC1 = list(listC)
            listC1.append(label_i)
            listC3.remove(label_j)
            listC2 = list(listC)
            listC3 = list(listC)
            listC3.append(label_i)

            fsetC1 = frozenset(listC1)
            fsetC2 = frozenset(listC2)
            fsetC3 = frozenset(listC3)

            key_old_B1_C1 = key.copy(A=fsetA_prime,B=fsetB1,C=fsetC1)
            key_old_B1_C2 = key.copy(A=fsetA_prime,B=fsetB1,C=fsetC2)
            key_old_B2_C1 = key.copy(A=fsetA_prime,B=fsetB2,C=fsetC1)
            key_old_B2_C2 = key.copy(A=fsetA_prime,B=fsetB2,C=fsetC2)

            key_old_B3_C1= key.copy(A=fsetA_prime,B=fsetB3,C=fsetC1)
            key_old_B3_C2= key.copy(A=fsetA_prime,B=fsetB3,C=fsetC2)
            key_old_B1_C3= key.copy(A=fsetA_prime,B=fsetB1,C=fsetC3)
            key_old_B2_C3= key.copy(A=fsetA_prime,B=fsetB2,C=fsetC3)

            key_old_B3_C3= key.copy(A=fsetA_prime,B=fsetB3,C=fsetC3)
        
            tempMakowskiTable[key]['NumModel'] = node1.markowskiTable[key_old_B1_C1]['NumModel'] + node1.markowskiTable[key_old_B1_C2]['NumModel'] + node1.markowskiTable[key_old_B2_C1]['NumModel'] + node1.markowskiTable[key_old_B2_C2]['NumModel'] - node1.markowskiTable[key_old_B3_C1]['NumModel'] - node1.markowskiTable[key_old_B3_C2]['NumModel'] - node1.markowskiTable[key_old_B1_C3]['NumModel'] - node1.markowskiTable[key_old_B2_C3]['NumModel'] + node1.markowskiTable[key_old_B3_C3]['NumModel']

        elif label_j in key['C']:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            key_old = key.copy(A= fsetA)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key_old]['NumModel']
        else:
            listA = list(key['A'])
            listA.append(label_i)
            fsetA=frozenset(listA)
            listC=list(key['C'])
            listC.append(label_j)
            fsetC=frozenset(listC)
            key_old_1 = key.copy(A=fsetA,C=fsetC)
            key_old_2 = key.copy(C=fsetC)
            tempMakowskiTable[key]['NumModel']=node1.markowskiTable[key]['NumModel']+node1.markowskiTable[key_old_1]['NumModel']-node1.markowskiTable[key_old_2]['NumModel']
    node1.markowskiTable=copy.deepcopy(tempMakowskiTable)

IsClause={'1':'clause','2':'clause','3':'variable','4':'variable'}
testNode1 = Node()
testNode1.kTable[0]='1'
testNode2=Node()
testNode2.kTable[1]='2'
testNode3 = Node()
testNode3.kTable[2]='3'
testNode4 = Node()
testNode4.kTable[3]='4'
initialMarkowskiforLeafNode(testNode1)
initialMarkowskiforLeafNode(testNode2)
initialMarkowskiforLeafNode(testNode3)
initialMarkowskiforLeafNode(testNode4)

unionNode = disjointUnion(testNode1,testNode2)
unionNode = disjointUnion(testNode3,unionNode)
unionNode = disjointUnion(testNode4,unionNode)

n_positive(unionNode,1,2)
p_operation(unionNode,1,0)
p_operation(unionNode,3,2)
n_positive(unionNode,0,2)

printNumModel(unionNode)