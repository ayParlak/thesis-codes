// dcwd-decode.c

/************************************************************************
*  This is decoder for directed graph by Aykut Parlak (June 6 2016)     *
*    modified code from cwd-encode by Marijn Heule (October 15th, 2013) *
************************************************************************/

#include <stdio.h>
#include <stdlib.h>

//int **edge, **lookup, nEdges, var_n, nSteps;//**arc,nArcs
int **arc, **lookup, nArcs, var_n, nSteps;
int *row, *column;
int *answer;

int *component, *group;

/*
void addEdge (int vertexU, int vertexV) {
    if (!edge[ vertexU ][ vertexV ])
      edge[ vertexU ][ vertexV ] = edge[ vertexV ][ vertexU ] = ++nEdges; }
*/

void addArc (int vertexU, int vertexV) 
{
   if (!arc[ vertexU ][ vertexV ]) 
    arc[ vertexU ][ vertexV ] = ++nArcs; 
}


int main (int argc, char **argv) {
    int c, i, j, k, l, m, tmp, lit;
    int u, v, w, x, t;
    //int nEdgeGraph, nVars, nClauses = 0, nGroups;
    int nArcGraph, nVars, nClauses = 0, nGroups;
    int vertexU, vertexV;
    int max, count;

    if (argc != 3) { printf("wrong input: ./dcwd_decode GRAPH SOLUTION\n"); exit(0); }

    FILE *graph    = fopen (argv[1], "r");
    FILE *solution = fopen (argv[2], "r");

    /*
    do 
    { 
      tmp = fscanf (graph, " p edge %i %i \n", &var_n, &nEdgeGraph);
      if (tmp > 0 && tmp != EOF) break; 
      tmp = fscanf (graph, "%*s\n"); 
    }
    while (tmp != 2 && tmp != EOF);
   */
   do 
   { 
        tmp = fscanf (graph, " p arc %i %i \n", &var_n, &nArcGraph);
        if (tmp > 0 && tmp != EOF) break; 
        tmp = fscanf (graph, "%*s\n"); 
   }
   while (tmp != 2 && tmp != EOF);
   

    nGroups = (var_n * (var_n - 1)) / 2;

    answer = (int *) malloc (sizeof(int) * 2 * var_n * var_n * var_n);
    for (i = 0; i < 2 * var_n * var_n * var_n; ++i)
      answer[ i ] = 0;

    max = 0;
    count = 0;
    
    do 
    { 
      tmp = fscanf (solution, " %i ", &lit);
         if (tmp == 0) tmp = fscanf (solution, "%*s ");
         else if (lit == 0) break;
         else 
         {
           if (abs(lit) > max) max = abs(lit);
           if (lit > 0) answer[ lit ] = 1;
         }
    }
    while (tmp != 2 && tmp != EOF);
    fclose (solution);

    tmp = max / var_n;
    i = var_n;

    continueloop:;
    i--;
    for (; i > 0; i--)
      if( !(tmp % (var_n + i - 1)) ) break;

    nSteps = tmp / (var_n + i - 1);
    for (j = 1; j <= nGroups; j++)
      if (answer[ j ] == 1) goto continueloop;

    tmp = 0;
    for (i = 1; ; i++) {
      int flag = 1;
      for (j = 1; j <= nGroups; j++)
        flag &= answer[ ++tmp ];
      if (flag & !answer[ tmp+1]) break;
    }

    nSteps = i;

    printf("c number of steps %i\n", nSteps);

    tmp = 0;
    for (i = 1; i <= nSteps; i++) {
      for (j = 1; j <= nGroups; j++)
        printf("%i", answer[ ++tmp ]);
      printf("\n");
    }

    printf("\n");

    for (i = 1; i <= nSteps; i++) {
      for (j = 1; j <= nGroups; j++)
        printf("%i", answer[ ++tmp ]);
      printf("\n");
    }

   int connected[ var_n + 1 ][ var_n + 1 ];
   int printed[ var_n + 1 ];

   for (j = 1; j <= var_n; j++) {
     printed[ j ] = 0;
     for (k = 1; k <= var_n; k++)
       connected [ j ][ k ] = nSteps; }

   for (i = 1; i <= nSteps; i++)
     for (j = 1; j <= var_n; j++) {
       tmp = (i - 1) * nGroups;
       for (k = 1; k < j; k++) tmp += var_n - k;
       for (k = j + 1; k <= var_n; k++) {
         if ((connected[j][k] == nSteps) &&
             (answer[tmp + k - j]))
           connected[j][k] = connected[k][j] = i - 1;
       }
     }

   int current = 1, next, step;
   printf("&&");
   for (j = 1; j < var_n; j++) {
     printf("%c", 'a' + current - 1);
     printed[ current ] = 1;
     step = nSteps + 1;
     for (k = 1; k <= var_n; k++)
       if (!printed[k] && current != k)
         if (connected[ current ][ k ] < step)
           next = k, step = connected[ current ][ k ];
     printf("^{%i}", step);
     current = next;
   }
   printf("%c : \\ \n", 'a' + current - 1);

   // print groups

   for (j = 1; j <= var_n; j++) {
     printed[ j ] = 0;
     for (k = 1; k <= var_n; k++)
       connected [ j ][ k ] = nSteps; }

   for (i = 1; i <= nSteps; i++)
     for (j = 1; j <= var_n; j++) {
       tmp = (nSteps + i - 1) * nGroups;
       for (k = 1; k < j; k++) tmp += var_n - k;
       for (k = j + 1; k <= var_n; k++) {
         if ((connected[j][k] == nSteps) &&
             (answer[tmp + k - j]))
           connected[j][k] = connected[k][j] = i - 1;
       }
     }

   current = 1;
   printf("&&");
   for (j = 1; j < var_n; j++) {
     printf("%c", 'a' + current - 1);
     printed[ current ] = 1;
     step = nSteps + 1;
     for (k = 1; k <= var_n; k++)
       if (!printed[k] && current != k)
         if (connected[ current ][ k ] < step)
           next = k, step = connected[ current ][ k ];
     printf("^{%i}", step);
     current = next;
   }
   printf("%c.\n", 'a' + current - 1);

   for (i = 1; i <= nSteps; i++) {
     for (j = 1; j <= var_n; j++) printed[ j ] = 0;

     printf("c ");
     for (j = 1; j <= var_n; j++) {
       if (!printed[ j ]) {
         printed[ j ] = 1;
         printf("{{%i", j);

         int g = (nSteps + i - 1) * nGroups;
         for (m = 1; m < j; m++) g += var_n - m;
         for (m = j + 1; m <= var_n; m++)
           if (answer[ g + m - j ])
             if (!printed[ m ]) {
               printed[ m ] = 1;
               printf(",%i", m); }

         tmp = (i-1) * nGroups;
         for (k = 1; k < j; k++) tmp += var_n - k;
         for (k = j + 1; k <= var_n; k++) {
           if (answer[ tmp + k - j ]) {
             if (!printed[ k ]) {
               printed[ k ] = 1;
               printf("}{%i", k);

         g = (nSteps + i - 1) * nGroups;
         for (m = 1; m < k; m++) g += var_n - m;
         for (m = k + 1; m <= var_n; m++)
           if (answer[ g + m - k ])
             if (!printed[ m ]) {
               printed[ m ] = 1;
               printf(",%i", m); }

             }
           }
         }
         printf("}}");
       }
     }
     printf("\n");
   }

/*
    edge = (int **) malloc (sizeof (int*) * (var_n+1));
    for (u = 1; u <= var_n; u++) edge[u] = (int *) malloc (sizeof (int) * (var_n+1) );

    for (u = 1; u <= var_n; u++)
      for (v = 1; v <= var_n; v++)
        edge[u][v] = 0;

    for (i = 1; i <= nEdgeGraph; i++) {
      fscanf (graph, " e %i %i \n", &vertexU, &vertexV);
      if (vertexU < vertexV) addEdge (vertexU, vertexV);
      else                   addEdge (vertexV, vertexU); }

    component = (int *) malloc (sizeof(int) * var_n * var_n * var_n);
    group     = (int *) malloc (sizeof(int) * var_n * var_n * var_n);

    row       = (int *) malloc (sizeof(int) * var_n * (var_n-1) / 2);
    column    = (int *) malloc (sizeof(int) * var_n * (var_n-1) / 2);

    c = 0;
    tmp = 0;
    for (i = 0; i < var_n; ++i) {
      for (j = i + 1; j < var_n; ++j) {
         row   [ tmp ] = j;
         column[ tmp ] = c;
         tmp++;
      }
      c++;
    }

    int n = var_n;

    for (i = 0; i <= var_n * var_n * nSteps; ++i)
      component[ i ] = group[ i ] = 0;

    for (i = 0; i <= nSteps; ++i)
      for (j = 0; j < var_n; ++j)
         component[ i * n * n + j * n + j ] = group[ i * n * n + j * n + j ] = 1;

    tmp = (nSteps + 1) * var_n * (var_n - 1) / 2;
    for (i = 1; i <= max; i++) {
      int t = (i - 1) / ((var_n) * (var_n-1) / 2);
      int m = (i - 1) % ((var_n) * (var_n-1) / 2);
      if (t < var_n) {
        component[ t * var_n * var_n + var_n * column[ m ] + row[ m ] ] = answer[       i ];
        group    [ t * var_n * var_n + var_n * column[ m ] + row[ m ] ] = answer[ tmp + i ];
      }
      printf("%i %i %i %i %i\n", t, m, row[ m ], column[ m ], i);
    }

    for (i = 0; i <= nSteps; ++i)
      for (j = 0; j < var_n; ++j)
        for (k = j + 1; k < var_n; ++k)
          if ( (component[ i     * n * n + j * n + k ] == 0) &&
               (component[ (i+1) * n * n + j * n + k ] == 1) &&
               (edge[ j+1 ][ k + 1]) ) {
            printf("c adding edge %i %i in step %i, requires ", j + 1, k + 1, i );
              for (l = 0; l < var_n; ++l)
                for (m = 0; m < var_n; ++m) {
                  int mina = j, maxa = l, minb = k, maxb = m;
                  if ( (l == m) ) continue;
                  if (mina > maxa) { mina = l; maxa = j; }
                  if (minb > maxb) { minb = m; maxb = k; }
                  if ( group[ (i+1) * n * n + mina * n + maxa ] &&
                       group[ (i+1) * n * n + minb * n + maxb ]) {
		    printf("(%i %i) ", l + 1, m  + 1);
                    if (edge[ l+1 ][ m + 1] == 0) printf(" ERROR ");
                  }
                }

            printf("\n");
          }

    for (i = 0; i <= nSteps; ++i) {
      for (j = 0; j < var_n; ++j) {
        for (k = 0; k < var_n; ++k)
          printf("%i", component[ i * var_n * var_n + j * var_n + k ] );
	printf("    ");
        for (k = 0; k < var_n; ++k)
          printf("%i", group[ i * var_n * var_n + j * var_n + k ] );

        printf("\n"); }
      printf("\n"); }
*/
}